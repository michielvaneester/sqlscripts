USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren SET Leeftijd = 9
WHERE Soort = 'hond' AND Baasje = 'Christiane';

UPDATE Huisdieren SET Leeftijd = 9
WHERE Soort = 'kat' AND Baasje = 'Bert';
SET SQL_SAFE_UPDATES = 1;