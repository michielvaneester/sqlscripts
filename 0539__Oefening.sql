USE ModernWays;
SELECT 
AVG(Leeftijd) AS "Gemiddelde leeftijd",
MAX(Leeftijd) AS "Oudste dier",
COUNT(*) AS "Hoeveel dieren" 
FROM Huisdieren;