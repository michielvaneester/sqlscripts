USE ModernWays;
ALTER TABLE Huisdieren ADD COLUMN Geluid VARCHAR(20);

SET SQL_SAFE_UPDATES = 0;
UPDATE Huisdieren
SET Geluid = 'WAF!'
WHERE Soort = 'hond';

UPDATE Huisdieren
SET Geluid = 'miauwww...'
WHERE Soort = 'kat';
SET SQL_SAFE_UPDATES = 1;